/*
Base code build by Iwan Boksebeld, thanks Iwan!
*/
#include "macgrid.h"

#include <Eigen/IterativeLinearSolvers>
#include <PolyVox/MarchingCubesSurfaceExtractor.h>
#include <PolyVox/Mesh.h>
#include <cmath>
#include <functional>
#include <igl/floor.h>
#include <iostream>

using namespace PolyVox;
using namespace Eigen;
using namespace std;

MaCGrid::MaCGrid(const double _h, const double _density)
	: h(_h), density(_density), marker_particles(0, 3),
	  volData(Region(Vector3DInt32(-100, 0, -100), Vector3DInt32(100, 100, 100)))
{
	volData.setBorderValue(GridCell(Vector3i::Zero(), -1, SOLID));

	Region region = volData.getEnclosingRegion();
	int32_t z, y, x;
#pragma omp parallel for private(z)
	for (z = region.getLowerZ(); z < region.getUpperZ(); z++)
#pragma omp parallel for private(y) shared(z)
		for (y = region.getLowerY(); y < region.getUpperY(); y++)
#pragma omp parallel for private(x) shared(z, y)
			for (x = region.getLowerX(); x < region.getUpperX(); x++)
			{
				auto &cell = volData.getVoxelRef(x, y, z);
				cell.coord << x, y, z;
			}
}

void MaCGrid::addParticles(const Eigen::MatrixXd &positions)
{
	int oldRows = marker_particles.rows();
	marker_particles.conservativeResize(oldRows + positions.rows(), 3);
	marker_particles.block(oldRows, 0, positions.rows(), 3) = positions;
}

void MaCGrid::simulate(const double timestep)
{
	updateGrid();
	advanceField(timestep);
	moveParticles(timestep);
}

void MaCGrid::displayFluid(igl::opengl::glfw::Viewer &viewer, const int offset)
{
	CustomController controller;
	auto mesh = extractMarchingCubesMesh(&volData, volData.getEnclosingRegion(), controller);
	auto decoded = decodeMesh(mesh);
	MatrixXd V(decoded.getNoOfVertices(), 3);
	MatrixXi T(decoded.getNoOfIndices() / 3, 3);
	for (int i = 0; i < V.rows(); i++)
	{
		auto vertex = decoded.getVertex(i).position +
					  (Vector3DFloat)volData.getEnclosingRegion().getLowerCorner();
		V.row(i) << vertex.getX(), vertex.getY(), vertex.getZ();
	}
	for (int i = 0; i < decoded.getNoOfIndices(); i++)
		T(i / 3, i % 3) = (int)decoded.getIndex(i);

	RowVector3d fluidColor(0.82, 0.82, 0.82);
	auto &viewData = viewer.data_list[offset];
	viewData.clear();

	// adding texture

	Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> R;
	Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> G;
	Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic> B;
	YImage src;
	src.load("SmokeShape.png");
	int width = src.width();
	int height = src.height();
	R.resize(src.width(), src.height());
	B.resize(R.rows(), R.cols());
	G.resize(R.rows(), R.cols());
	for (int r = 0; r < height; r++)
	{
		for (int c = 0; c < width; c++)
		{
			const auto &p = src.at(r, c);
			R(r, R.cols() - 1 - c) = p.r;
			G(r, R.cols() - 1 - c) = p.g;
			B(r, R.cols() - 1 - c) = p.b;
		}
	}
	viewData.set_texture(R, G, B);

	viewData.set_mesh(V, T);
	viewData.set_face_based(true);
	viewData.set_colors(fluidColor);
	viewData.set_uv(viewData.V_uv);
	// viewData.show_texture = true;
	viewData.show_lines = false;
}

void MaCGrid::updateGrid()
{
	Region region = volData.getEnclosingRegion();
	// This three-level for loop iterates over every voxel in the volume
	// Reset all cells. Everything that's not solid is now air,
	// on layer -1.
	int32_t z, y, x;
#pragma omp parallel for private(z)
	for (z = region.getLowerZ(); z < region.getUpperZ(); z++)
#pragma omp parallel for private(y) shared(z)
		for (y = region.getLowerY(); y < region.getUpperY(); y++)
#pragma omp parallel for private(x) shared(z, y)
			for (x = region.getLowerX(); x < region.getUpperX(); x++)
			{
				auto &cell = volData.getVoxelRef(x, y, z);
				cell.layer = -1;
				cell.idx = -1;
				cell.mask.setZero();
				if (cell.type != SOLID)
					cell.type = AIR;
			}

	fluidCells.clear();

	for (int i = 0; i < marker_particles.rows(); i++)
	{
		RowVector3d loc = marker_particles.row(i);
		RowVector3i coord;
		igl::floor(loc, coord);

		// Skip if outside of the grid:
		if (!volData.getEnclosingRegion().containsPoint(
				Vector3DInt32(coord.x(), coord.y(), coord.z())))
			continue;

		auto &cell = volData.getVoxelRef(coord.x(), coord.y(), coord.z());
		if (cell.type == FLUID)
			// Cell already added
			continue;

		if (cell.type != SOLID)
		{
			// Marker-particle on a non-solid cell means
			// this cell now contains fluid:
			cell.layer = 0;
			cell.type = FLUID;
			// Identify where in the list this cell is
			// (to speed up future processes):
			cell.mask.setConstant(1);
			cell.idx = fluidCells.size();
			fluidCells.insert(&cell);
		}
	}

	borderCells.clear();

	for (auto cell : fluidCells)
		for (int c = 0; c < 3; ++c)
		{
			auto coord = cell->coord;
			coord(c) += 1;

			if (!volData.getEnclosingRegion().containsPoint(
					Vector3DInt32(coord.x(), coord.y(), coord.z())))
				continue;
			auto &neigh = volData.getVoxelRef(coord.x(), coord.y(), coord.z());
			if (neigh.type != FLUID)
			{
				neigh.mask(c) = 1;
				borderCells.insert(&neigh);
			}
		}
}

void MaCGrid::advanceField(const double timestep)
{
	applyConvection(timestep);
	externalForces(timestep);
	// The effects of viscosity are negligible in gases especially on coarse grids where numerical
	// dissipation dominates physical viscosity and molecular diffusion.
	calcPressure(timestep);
	addVorticity();
	fixSolidCellVelocities();
}

void MaCGrid::applyConvection(const double timestep)
{
#pragma omp parallel
	for (auto cell : fluidCells)
		cell->convect(*this, timestep);

#pragma omp parallel
	for (auto cell : borderCells)
		cell->convect(*this, timestep);

#pragma omp parallel
	for (auto cell : fluidCells)
	{
		cell->u = cell->u_temp;
		cell->u_temp.setZero();
	}

#pragma omp parallel
	for (auto cell : borderCells)
		cell->u = cell->u_temp;
}

void MaCGrid::externalForces(const double timestep)
{
	// fbuoy = -alpha * densitysmoke * z + beta (temperature - ambient temperature) * z
	const int alpha = 0.1;
	const int beta = 0.3;

	// smoke density
	double smokeDensity = 0.5;

	const double T = 1;
	const double ambT = 0.5;

	Vector3d f;
	f << timestep * (-alpha * smokeDensity + beta * (T - ambT)),
		timestep * (-alpha * smokeDensity + beta * (T - ambT)), 1;

#pragma omp parallel
	for (auto cell : fluidCells)
		cell->u += f;

#pragma omp parallel
	for (auto cell : borderCells)
		cell->u += cell->mask.cwiseProduct(f);
}

void MaCGrid::calcPressure(const double timestep)
{
	// in assignment 3, main addmesh poissonratio is 0.5
	double poissonRatio = 0.5;

	Vector3d p;
	p << poissonRatio * timestep, poissonRatio * timestep, 0;

	// minus p because u - delta p + force in the paper
#pragma omp parallel
	for (auto cell : fluidCells)
		cell->u -= p;

#pragma omp parallel
	for (auto cell : borderCells)
		cell->u -= cell->mask.cwiseProduct(p);
}

void MaCGrid::addVorticity()
{
	Vector3d vorticityW;
	Vector3d nabla;
	nabla << 1, 1, 1;
	Vector3d n;
	Vector3d N;
	Vector3d fConf;

#pragma omp parallel
	for (auto cell : fluidCells)
	{
		vorticityW = nabla.cwiseProduct(cell->u);
		n = vorticityW.size() * nabla;
		N = n / n.size();
		fConf = h * (N.cwiseProduct(vorticityW));
		cell->u += fConf;
	}

#pragma omp parallel
	for (auto cell : borderCells)
	{
		vorticityW = nabla.cwiseProduct(cell->u);
		n = vorticityW.size() * nabla;
		N = n / n.size();
		fConf = h * (N.cwiseProduct(vorticityW));
		cell->u += cell->mask.cwiseProduct(fConf);
	}

}

void MaCGrid::fixSolidCellVelocities()
{
	Region region = volData.getEnclosingRegion();
	int32_t z, y, x;
#pragma omp parallel for private(z)
	for (z = region.getLowerZ(); z < region.getUpperZ(); z++)
#pragma omp parallel for private(y) shared(z)
		for (y = region.getLowerY(); y < region.getUpperY(); y++)
#pragma omp parallel for private(x) shared(z, y)
			for (x = region.getLowerX(); x < region.getUpperX(); x++)
			{
				auto &cell = volData.getVoxelRef(x, y, z);
				for (int c = 0; c < 3; ++c)
				{
					auto coord = cell.coord;
					coord(c) -= 1;
					auto &neigh = volData.getVoxel(coord.x(), coord.y(), coord.z());
					if (neigh.type == SOLID && cell.u(c) < 0)
						cell.u(c) = 0;
				}
			}
}

void MaCGrid::moveParticles(const double timestep)
{
	int i;
#pragma omp parallel for schedule(runtime) private(i)
	for (i = 0; i < marker_particles.rows(); i++)
		marker_particles.row(i)
			<< traceParticle(marker_particles.row(i).transpose(), timestep).transpose();
}

template <class input_t, class output_t, class step_t>
output_t rk3(std::function<output_t(input_t)> f, input_t x, step_t h)
{
	auto k_1 = f(x);
	auto k_2 = f(x + k_1 * h * 1.0 / 2);
	auto k_3 = f(x + k_2 * h * 3.0 / 4);
	return (k_1 * 2 + k_2 * 3 + k_3 * 4) * h * 1.0 / 9;
}

Vector3d MaCGrid::traceParticle(double x, double y, double z, double t) const
{
	return traceParticle({x, y, z}, t);
}

Vector3d MaCGrid::traceParticle(const Vector3d &p, double t) const
{
	Vector3d diff =
		rk3<Vector3d, Vector3d, double>([&](Vector3d pos) { return getVelocity(pos); }, p, t);

	Vector3d vel1 = getVelocity(p);
	Vector3d vel2 = getVelocity(p + t / 2 * vel1);
	Vector3d vel3 = getVelocity(p + t * 3 / 4 * vel2);
	return p + (vel1 * 2 + vel2 * 3 + vel3 * 4) * t / 9;
}

Vector3d MaCGrid::getVelocity(const Vector3d &pos) const
{
	return {getInterpolatedValue(pos.x(), pos.y() - 0.5, pos.z() - 0.5, 0),
			getInterpolatedValue(pos.x() - 0.5, pos.y(), pos.z() - 0.5, 1),
			getInterpolatedValue(pos.x() - 0.5, pos.y() - 0.5, pos.z(), 2)};
}

double MaCGrid::getInterpolatedValue(double x, double y, double z, int index) const
{
	double i = std::floor(x);
	double j = std::floor(y);
	double k = std::floor(z);
	double x_frac = i - x;
	double y_frac = j - y;
	double z_frac = k - z;

	double value_000 = volData.getVoxel(i, j, k).u(index);
	double value_100 = volData.getVoxel(i + 1, j, k).u(index);
	double value_010 = volData.getVoxel(i, j + 1, k).u(index);
	double value_110 = volData.getVoxel(i + 1, j + 1, k).u(index);
	double value_001 = volData.getVoxel(i, j, k + 1).u(index);
	double value_101 = volData.getVoxel(i + 1, j, k + 1).u(index);
	double value_011 = volData.getVoxel(i, j + 1, k + 1).u(index);
	double value_111 = volData.getVoxel(i + 1, j + 1, k + 1).u(index);

	double value_00 = (1 - x_frac) * value_000 + x_frac * value_100;
	double value_10 = (1 - x_frac) * value_010 + x_frac * value_110;
	double value_01 = (1 - x_frac) * value_001 + x_frac * value_101;
	double value_11 = (1 - x_frac) * value_011 + x_frac * value_111;

	double value_0 = (1 - y_frac) * value_00 + y_frac * value_10;
	double value_1 = (1 - y_frac) * value_01 + y_frac * value_11;

	return (1 - z_frac) * value_0 + z_frac * value_1;
}

GridCell::GridCell() : type(AIR), layer(-1), idx(-1)
{
	coord.setConstant(INT32_MAX);
	u.setZero();
	u_temp.setZero();
}

GridCell::GridCell(const Eigen::Vector3i &_coord, const int _layer, const CellType _type)
	: coord(_coord), layer(_layer), type(_type), idx(-1)
{
	u.setZero();
	u_temp.setZero();
}

void GridCell::convect(const MaCGrid &grid, const double timestep)
{
	Vector3d u_x = grid.traceParticle(coord.x(), coord.y() + 0.5, coord.z() + 0.5, -timestep);
	Vector3d u_y = grid.traceParticle(coord.x() + 0.5, coord.y(), coord.z() + 0.5, -timestep);
	Vector3d u_z = grid.traceParticle(coord.x() + 0.5, coord.y() + 0.5, coord.z(), -timestep);
	u_temp << grid.getInterpolatedValue(u_x.x(), u_x.y() - 0.5, u_x.z() - 0.5, 0),
		grid.getInterpolatedValue(u_y.x() - 0.5, u_y.y(), u_y.z() - 0.5, 1),
		grid.getInterpolatedValue(u_z.x() - 0.5, u_z.y() - 0.5, u_z.z(), 2);
	u_temp = u_temp.cwiseProduct(mask);
}
